import 'package:flutter/material.dart';

import 'package:fun_quiz/splash/splash_page.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "FunQuiz",
      home: SplashPage(),
    );
  }
}
