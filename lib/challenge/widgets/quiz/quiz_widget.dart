import 'package:flutter/material.dart';
import 'package:fun_quiz/challenge/widgets/answer/answer_widget.dart';
import 'package:fun_quiz/core/core.dart';
import 'package:fun_quiz/shared/models/answer_model.dart';
import 'package:fun_quiz/shared/models/question_model.dart';

class QuizWidget extends StatefulWidget {
  final QuestionModel question;
  final ValueChanged<bool> onSelected;

  const QuizWidget({
    Key? key,
    required this.question,
    required this.onSelected,
  }) : super(key: key);

  @override
  _QuizWidgetState createState() => _QuizWidgetState();
}

class _QuizWidgetState extends State<QuizWidget> {
  int indexSelected = -1;

  AnswerModel answer(int index) => widget.question.answers[index];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(height: 64),
          Text(
            widget.question.title,
            style: AppTextStyles.heading,
          ),
          SizedBox(height: 24),
          ...widget.question.answers
              .asMap()
              .entries
              .map(
                (entries) => AnswerWidget(
                  answer: answer(entries.key),
                  isSelected: indexSelected == entries.key,
                  onTap: (answerRightIsSelected) {
                    indexSelected = entries.key;

                    setState(() {});

                    Future.delayed(Duration(seconds: 1))
                        .then((_) => widget.onSelected(answerRightIsSelected));
                  },
                  disabled: indexSelected != -1,
                ),
              )
              .toList(),
        ],
      ),
    );
  }
}
