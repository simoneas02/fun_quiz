import 'package:flutter/material.dart';

import 'package:fun_quiz/challenge/challenge_page.dart';
import 'package:fun_quiz/core/core.dart';
import 'package:fun_quiz/home/home_controller.dart';
import 'package:fun_quiz/home/hone_state.dart';
import 'package:fun_quiz/home/widgets/app_bar/app_bar_widget.dart';
import 'package:fun_quiz/home/widgets/level_button/level_button_widget.dart';
import 'package:fun_quiz/home/widgets/quiz_card/quiz_card_wodget.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final controller = HomeController();

  @override
  void initState() {
    super.initState();

    controller.getUser();
    controller.getQuizzes();

    controller.stateNotifier.addListener(() {
      setState(() {});
    });
  }

  Widget build(BuildContext context) {
    if (controller.state == HomeState.success) {
      return Scaffold(
        appBar: AppBarWidget(
          user: controller.user!,
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              SizedBox(height: 24),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  LevelButtonWidget(label: 'Fácil'),
                  LevelButtonWidget(label: 'Médio'),
                  LevelButtonWidget(label: 'Difícil'),
                  LevelButtonWidget(label: 'Perito'),
                ],
              ),
              SizedBox(height: 24),
              Expanded(
                child: GridView.count(
                  crossAxisSpacing: 16,
                  mainAxisSpacing: 16,
                  crossAxisCount: 2,
                  children: controller.quizzes!
                      .map((quiz) => QuizCardWidget(
                            title: quiz.title,
                            percent:
                                quiz.questionAnswered / quiz.questions.length,
                            completed:
                                "${quiz.questionAnswered}/${quiz.questions.length}",
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ChallengePage(
                                            questions: quiz.questions,
                                            title: quiz.title,
                                          )));
                            },
                          ))
                      .toList(),
                ),
              )
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
        body: Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(AppColors.darkGreen),
          ),
        ),
      );
    }
  }
}
